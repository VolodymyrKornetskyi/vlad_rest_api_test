export default function auth(req, res, next) {
  if (req.query.token === 'customToken') {
    return next();
  }
  throw res.send(401, 'Unatorized');
}
