import http from 'http';
import app from './app';
import dbConnection from './db/dbConnection';

const port = 3000;

const server = http.createServer(app);

(async () => {
  try {
    await dbConnection.dbConnect();
    console.log('Connected to MongoDB.');
    server.listen(
      port,
      (err) => console.log(err ? `Something bad happened. Error: ${err}` : `Server is listening on ${port}`),
    );
  } catch (e) {
    console.log('Can\'t start the service. Error', e);
  }
})();

