import { getAllTeachers, createTeacher } from '../db/teachersService';

export const getAll = async (req, res) => {
  const teachers = await getAllTeachers();
  res.send(teachers);
};

export const create = async (req, res) => {
  const teacher = await createTeacher(req.body);
  res.status(201);
  res.send(teacher);
};
