import Teacher from '../models/teacher';

export async function getAllTeachers() {
  return Teacher.Model.find();
}

export function getTeacherById(id) {
  return Teacher.Model.findById(id);
}

export function createTeacher(teacher) {
  return (new Teacher.Model(teacher)).save();
}
