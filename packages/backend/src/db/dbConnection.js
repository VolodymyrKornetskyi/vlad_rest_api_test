import mongoose from 'mongoose';

const dbConnect = async () => mongoose.connect('mongodb://cookcook:123123@0.0.0.0', { useNewUrlParser: true });

const closeConnect = () => {
  mongoose.connection.close().then(() => {
    console.log('Connection close.');
  }).catch((e) => {
    console.log(`Not closed. ERROR:\n', ${e}`);
  });
};

export default {
  dbConnect,
  closeConnect,
};
