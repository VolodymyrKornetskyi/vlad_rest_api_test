import express from 'express';
import {getAll, create} from '../../controllers/teachers';
import auth from '../../middlewares/auth';

const router = express.Router();

router.get('/', auth, getAll);
router.post('/create', auth, create)

export default router;
