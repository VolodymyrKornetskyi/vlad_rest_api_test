import express from 'express';
import routes from './routes/index';

const app = express();

// respond with "hello world" when a GET request is made to the homepage
app.use('/', routes);

export default app;
