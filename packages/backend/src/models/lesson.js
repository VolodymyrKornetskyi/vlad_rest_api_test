import mongoose from 'mongoose';
import Teacher from './teacher';
import Classroom from './classroom';
import Group from './group';

const Schema = mongoose.Schema({
  id: mongoose.Schema.Types.ObjectId,
  theme: String,
  teacher: Teacher.Schema,
  classroom: Classroom.Schema,
  specification: String,
  pairNumber: String,
  group: Group.Schema,
  added: Date,
  lastUpdate: Date,
});

const Model = mongoose.model('Lesson', Schema);

export default {
  Model,
  Schema,
};
