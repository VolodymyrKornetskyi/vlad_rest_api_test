import mongoose from 'mongoose';

const Schema = mongoose.Schema({
  id: mongoose.Schema.Types.ObjectId,
  name: String,
  surname: String,
  age: String,
  phone: String,
  email: String,
});

const Model = mongoose.model('Student', Schema);

export default {
  Model,
  Schema,
};
