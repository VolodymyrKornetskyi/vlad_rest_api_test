import mongoose from 'mongoose';

const Schema = mongoose.Schema({
  id: mongoose.Schema.Types.ObjectId,
  name: String,
  surname: String,
  age: String,
  specification: String,
  added: Date,
  lastUpdate: Date,
});

const Model = mongoose.model('Teacher', Schema);

export default {
  Model,
  Schema,
};
